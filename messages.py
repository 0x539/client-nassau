"""
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Permet de gérer les messages dans l’interface.
"""

import locale
import time
import json

import réseau


class Message:
    def __init__(self, ajustement_bc_messages, serveur):
        self.ajustement_bc_messages = ajustement_bc_messages
        self.serveur = serveur

        self.nombre_messages = 0
        self.message_actuel = 0
        self.messages = []

        self.mettre_à_jour()

    def mettre_à_jour(self):
        self.__mettre_à_jour_nombre_messages__()
        self.__mettre_à_jour_messages__()
        if self.nombre_messages == 0:
            self.ajustement_bc_messages.set_lower(0)
            self.ajustement_bc_messages.set_upper(0)
        else:
            self.ajustement_bc_messages.set_value(1)
            self.ajustement_bc_messages.set_lower(1)
            self.ajustement_bc_messages.set_upper(self.nombre_messages)

    def __mettre_à_jour_nombre_messages__(self):
        nombre_messages = self.serveur.obtenir_nombre_messages()
        if nombre_messages == None:
            return
        else:
            self.nombre_messages = nombre_messages

    def __mettre_à_jour_messages__(self):
        if self.nombre_messages == 0:
            return

        if self.nombre_messages == len(self.messages):
            message = self.serveur.obtenir_contenu_message(1)
            if self.__convertir_message__(message) != self.messages[0]:
                self.__demander_messages__()
            else:
                return

        elif self.nombre_messages < len(self.messages):
            self.__demander_messages__()

        elif self.nombre_messages > len(self.messages)\
                and len(self.messages) != 0:
            self.__demander_messages__()
        else:
            self.__demander_messages__()

    def __demander_messages__(self, numéro_message_départ=1):
        self.messages = []
        for i in range(numéro_message_départ, self.nombre_messages + 1):
            message = self.serveur.obtenir_contenu_message(i)
            if message != None:
                self.messages.append(self.__convertir_message__(message))

    def obtenir_nombre_messages(self):
        return self.nombre_messages

    def obtenir_message(self, numéro_message=1):
        if numéro_message > len(self.messages) or numéro_message < 1:
            return None
        else:
            return self.messages[numéro_message - 1]

    def __convertir_message__(self, message):
        if message == None:
            return

        dico = json.loads(message)
        type_mission = dico["type"]
        if type_mission == "espionnage":
            return self.__formater_espionnage__(dico)
        elif type_mission == "retour":
            return self.__formater_retour__(dico)
        elif type_mission == "colonisation réussie":
            return self.__formater_colonisation_fructueuse__(dico)
        elif type_mission == "colonisation infructueuse":
            return self.__formater_colonisation_infructueuse__(dico)
        # elif type_mission == "attaque":
        #     return self.__formater_attaque__(dico)
        else:
            return message

    def __obtenir_date_formatée__(self, message):
        date = message["date"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X", time.localtime(date))
        return date_formatée

    def __formater_espionnage__(self, message):
        date = message["date"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X", time.localtime(date))

        retour = "Rapport du " + date_formatée
        retour += " concernant un espionnage."
        retour += "\n\n"
        retour += "Nom de la cible : " + message["cible"] + "\n"
        île_cible = message["île cible"]
        retour += "Coordonnées de l’île cible : ("
        retour += str(île_cible["océan"]) + ", " + str(île_cible["secteur"])
        retour += ", " + str(île_cible["emplacement"]) + ")"
        retour += "\n\n"

        ressources = message["ressources"]
        if ressources["bois"] == -1:
            retour += "Vous n’avez obtenu aucune information."
            return retour
        retour += "## Ressources ##\n"
        retour += "Bois : " + str(ressources["bois"]) + "\n"
        retour += "Métal : " + str(ressources["métal"]) + "\n"
        retour += "Poudre à canon : " + str(ressources["poudre à canon"])
        retour += "\n\n"

        flotte = message["flotte"]
        if flotte["bateau espion"] == -1:
            return retour
        retour += "## Flotte ##\n"
        retour += "Bateau(x) espion(s) : " + \
            str(flotte["bateau espion"]) + "\n"
        retour += "Bateau(x) de transport léger : " + \
            str(flotte["bateau transport léger"]) + "\n"
        retour += "Bateau(x) de transport lourd : " + \
            str(flotte["bateau transport lourd"]) + "\n"
        retour += "Canonnière(s) : " + str(flotte["canonnière"]) + "\n"
        retour += "Frégate(s) : " + str(flotte["frégate"]) + "\n"
        retour += "Corvette(s) : " + str(flotte["corvette"]) + "\n"
        retour += "Brick(s) : " + str(flotte["brick"]) + "\n"
        retour += "Galion(s) : " + str(flotte["galion"]) + "\n"
        retour += "Navire(s) de ligne : " + str(flotte["navire de ligne"])
        retour += "\n\n"

        défense = message["défenses"]
        retour += "## Défenses ##\n"
        retour += "Mur(s) : " + str(défense["mur"]) + "\n"
        retour += "Canon(s) : " + str(défense["canon"]) + "\n"
        retour += "Mine(s) : " + str(défense["mine"]) + "\n"
        retour += "Mortier(s) : " + str(défense["mortier"]) + "\n"
        retour += "Fort(s) : " + str(défense["fort"])
        retour += "\n\n"

        bâtiments = message["bâtiments"]
        if bâtiments["forêt"] == -1:
            return retour
        retour += "## Bâtiments ##\n"
        retour += "Forêt : " + str(bâtiments["forêt"]) + "\n"
        retour += "Mine de métal : " + str(bâtiments["mine de métal"]) + "\n"
        retour += "Fabrique de poudre à canon : " + \
            str(bâtiments["fabrique poudre canon"]) + "\n"
        retour += "Quartier de travailleurs : " + \
            str(bâtiments["quartier travailleurs"]) + "\n"
        retour += "Hangar de bois : " + str(bâtiments["hangar bois"]) + "\n"
        retour += "Hangar de métal : " + str(bâtiments["hangar métal"]) + "\n"
        retour += "Poudrière : " + str(bâtiments["poudrière"]) + "\n"
        retour += "Chantier naval : " + str(bâtiments["chantier naval"]) + "\n"
        retour += "Laboratoire de recherche : " + \
            str(bâtiments["laboratoire recherche"]) + "\n"
        retour += "Camp de formation de travailleurs : " + \
            str(bâtiments["camp formation travailleurs"])
        retour += "\n\n"

        recherches = message["recherches"]
        retour += "## Recherches ##\n"
        retour += "Espionnage : " + str(recherches["espionnage"]) + "\n"
        retour += "Voilure : " + str(recherches["voilure"]) + "\n"
        retour += "Coque : " + str(recherches["coque"]) + "\n"
        retour += "Canons : " + str(recherches["canons"]) + "\n"
        retour += "Formation d’officiers : " + \
            str(recherches["formation d’officiers"])

        return retour

    def __formater_retour__(self, message):
        date = message["date"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X", time.localtime(date))

        retour = "Rapport du " + date_formatée
        retour += " concernant le retour d’une flotte."
        retour += "\n\n"

        île_départ = message["île départ"]
        île_retour = message["île retour"]

        retour += "Une flotte est rentrée de l’île ("
        retour += str(île_départ["océan"]) + ", " + str(île_départ["secteur"])
        retour += ", " + str(île_départ["emplacement"]) + ")"
        retour += " sur l’île ("
        retour += str(île_retour["océan"]) + ", " + str(île_retour["secteur"])
        retour += ", " + str(île_retour["emplacement"]) + ").\n"

        ressources = message["ressources"]
        if ressources["bois"] == 0 and ressources["métal"] == 0\
           and ressources["poudre à canon"] == 0:
            retour += "Cette flotte ne transportait aucune ressource."
            return retour

        retour += "Cette flotte transportait "
        retour += str(ressources["bois"]) + " de bois, "
        retour += str(ressources["métal"]) + " de métal, "
        retour += str(ressources["poudre à canon"]) + " de poudre à canon."
        retour += "\n\n"
        return retour

    def __formater_colonisation_fructueuse__(self, message):
        date = message["date"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X", time.localtime(date))

        retour = "Rapport du " + date_formatée
        retour += " concernant une colonisation fructueuse."
        retour += "\n\n"

        île = message["île"]

        retour += "La colonisation de l’île ("
        retour += str(île["océan"]) + " ; " + str(île["secteur"])
        retour += " ; " + str(île["emplacement"]) + ")"
        retour += " a réussie."
        return retour

    def __formater_colonisation_infructueuse__(self, message):
        date = message["date"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X", time.localtime(date))

        retour = "Rapport du " + date_formatée
        retour += " concernant une colonisation infructueuse."
        retour += "\n\n"

        île = message["île"]

        retour += "La colonisation de l’île ("
        retour += str(île["océan"]) + " ; " + str(île["secteur"])
        retour += " ; " + str(île["emplacement"]) + ")"
        retour += " a échoué. De ce fait, votre flotte "
        retour += "retourne d’où elle est venue.\n\n"
        return retour

    def __formater_attaque__(self, message):
        date = self.__obtenir_date_formatée__(message)

        retour = "Rapport du " + date
        retour += " concernant une attaque."
        retour += "\n\n"

        île = message["île cible"]
        retour += "L’île cible est ("
        retour += str(île["océan"]) + " ; " + str(île["secteur"])
        retour += " ; " + str(île["emplacement"]) + ")"

        if message["issue du combat"] == "victoire":
            retour += "Vous avez gagné le combat.\n"
            ressources = message["ressources obtenues"]
            retour += "Vous avez remporté " + ressources["bois"] + " bois, "
            retour += ressources["métal"] + " métal, "
            retour += ressources["poudre à canon"] + " poudre à canon.\n\n"
        elif message["issue du combat"] == "défaite":
            retour += "Vous avez perdu le combat.\n"
