"""
Auteur : Baptiste Bocquillon <pyth0n11@nybble.fr> 2016 à 2017
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016 à 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ce fichier contient l’interface du jeu Nassau.
"""

import réseau
from ressources import *
import messages
import expéditions
import json
import threading
from connexion import obtenir_identifiants
import time

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject

adresse_serveur = ("s2.vendémiaire.fr", 4284)


class Fenêtre:
    def __init__(self, pseudo, mot_de_passe):
        self.constructeur = Gtk.Builder()
        self.constructeur.add_from_file('ihm/nassau_interface.glade')

        self.fenêtre = self.constructeur.get_object("fenetre1")
        self.fenêtre.show_all()

        self.pseudo = pseudo
        self.mot_de_passe = mot_de_passe

        self.serveur = réseau.Serveur(adresse_serveur,
                                      self.pseudo, self.mot_de_passe)

        self.île_sélectionnée = 1
        self.obtenir_informations_sur_mes_îles()
        self.remplir_modèle_liste_île()

        self.récupérer_étiquettes()
        self.récupérer_bc()
        self.récupérer_ajustements()
        self.récupérer_boites_combinées()

        self.contenu_message = self.constructeur.get_object("contenu_message")
        self.tampon_message = Gtk.TextBuffer()
        self.contenu_message.set_buffer(self.tampon_message)

        self.message = messages.Message(self.constructeur.get_object(
            "ajustement_bc_messages"), self.serveur)
        self.bloc_notes = self.constructeur.get_object("pages")
        self.expéditions = expéditions.Expéditions(self.bloc_notes,
                                                   self.serveur, self.pseudo)

        threading.Thread(target=self.obtenir_constructions_en_cours).start()

        self.afficher_message()
        self.afficher_ressources()

        self.bâtiments = None
        self.afficher_bâtiments()

        self.afficher_navires()
        self.afficher_recherches()
        self.afficher_coût_production_navires()
        self.afficher_coût_production_défenses()
        self.afficher_défenses()
        self.afficher_vue_océan()

        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()
        self.afficher_informations_sur_île()

        self.constructeur.connect_signals(self)

        GObject.timeout_add_seconds(10, self.afficher_ressources)

        # À changer, il faudrait le faire uniquement quand cela est nécessaire.
        GObject.timeout_add_seconds(60, self.afficher_bâtiments)
        GObject.timeout_add_seconds(60, self.afficher_recherches)
        GObject.timeout_add_seconds(60, self.afficher_navires)
        GObject.timeout_add_seconds(60, self.afficher_défenses)
        GObject.timeout_add_seconds(60, self.message.mettre_à_jour)

        Gtk.main()

    def obtenir_informations_sur_mes_îles(self):
        informations = self.serveur.obtenir_informations_sur_mes_îles()
        if informations is None:
            return

        informations = json.loads(informations)
        self.nombre_îles = informations["nombre d’îles"]
        self.îles = []
        for i in range(1, self.nombre_îles + 1):
            self.îles.append(informations["île" + str(i)])

    def récupérer_étiquettes(self):
        self.récupérer_étiquettes_vue_ensemble()
        self.récupérer_étiquettes_ressources()
        self.récupérer_étiquettes_production()
        self.récupérer_étiquettes_bâtiments()
        self.récupérer_étiquettes_recherches()
        self.récupérer_étiquettes_coûts()
        self.récupérer_étiquettes_navires()
        self.récupérer_étiquettes_défenses()
        self.récupérer_étiquettes_îles()

    def récupérer_étiquettes_vue_ensemble(self):
        self.étiquette_constructions_en_cours = self.constructeur.get_object(
            "constructions_en_cours")
        self.informations_îles = self.constructeur.get_object(
            "informations_île_actuelle")

    def récupérer_étiquettes_ressources(self):
        self.bois = self.constructeur.get_object("bois")
        self.metal = self.constructeur.get_object("metal")
        self.poudre_a_canon = self.constructeur.get_object("poudre_a_canon")
        self.travailleurs = self.constructeur.get_object("travailleurs")

    def récupérer_étiquettes_production(self):
        self.production_bois = self.constructeur.get_object("production_bois")
        self.production_métal = self.constructeur.get_object(
            "production_métal")
        self.production_pàc = self.constructeur.get_object("production_pàc")
        self.production_travailleurs\
            = self.constructeur.get_object("production_travailleurs")

        self.production_bois_niveau_suivant\
            = self.constructeur.get_object("production_bois_niveau_suivant")
        self.production_métal_niveau_suivant\
            = self.constructeur.get_object("production_métal_niveau_suivant")
        self.production_pàc_niveau_suivant\
            = self.constructeur.get_object("production_pàc_niveau_suivant")
        self.production_travailleurs_niveau_suivant\
            = self.constructeur.get_object("production_travailleurs_niveau_suivant")

    def récupérer_ajustements(self):
        self.récupérer_ajustements_flotte()

    def récupérer_ajustements_flotte(self):
        self.ajustement_bc_flotte_espion = self.constructeur.get_object(
            "ajustement_bc_flotte_espion")
        self.ajustement_bc_flotte_frégate = self.constructeur.get_object(
            "ajustement_bc_flotte_frégate")
        self.ajustement_bc_flotte_brick = self.constructeur.get_object(
            "ajustement_bc_flotte_brick")
        self.ajustement_bc_flotte_ndl = self.constructeur.get_object(
            "ajustement_bc_flotte_ndl")
        self.ajustement_bc_flotte_tlo = self.constructeur.get_object(
            "ajustement_bc_flotte_tlo")
        self.ajustement_bc_flotte_canonnière\
            = self.constructeur.get_object("ajustement_bc_flotte_canonnière")
        self.ajustement_bc_flotte_corvette = self.constructeur.get_object(
            "ajustement_bc_flotte_corvette")
        self.ajustement_bc_flotte_galion = self.constructeur.get_object(
            "ajustement_bc_flotte_galion")
        self.ajustement_bc_flotte_tlé = self.constructeur.get_object(
            "ajustement_bc_flotte_tlé")
        self.ajustement_bc_flotte_bois = self.constructeur.get_object(
            "ajustement_bc_flotte_bois")
        self.ajustement_bc_flotte_métal = self.constructeur.get_object(
            "ajustement_bc_flotte_métal")
        self.ajustement_bc_flotte_pàc = self.constructeur.get_object(
            "ajustement_bc_flotte_pàc")

    def récupérer_boites_combinées(self):
        self.type_mission = self.constructeur.get_object("type_mission")
        self.modèle_type_mission = self.constructeur.get_object(
            "modèle_liste_type_mission")

    def lire_identifiants(self):
        try:
            fichier = open("compte", "r")
            identifiants = fichier.readline().rstrip().split()
            self.pseudo = identifiants[0]
            self.mot_de_passe = identifiants[1]
        except:
            print("Fichier de configuration incorrecte.")
            return False

    def demander_identifiants(self):
        return

    def afficher_ressources(self):
        self.ressources = self.serveur.récupérer_ressources(
            self.île_sélectionnée)

        self.bois.set_text("Bois : " + self.ressources[0])
        self.metal.set_text("Métal : " + self.ressources[1])
        self.poudre_a_canon.set_text("Poudre à canon : " + self.ressources[2])
        self.travailleurs.set_text("Travailleurs : " + self.ressources[3])

        self.borner_ajustements_flotte()

        return True

    def récupérer_étiquettes_bâtiments(self):
        self.forêt = self.constructeur.get_object("forêt")
        self.mine_de_métal = self.constructeur.get_object("mine_de_métal")
        self.fabrique_de_poudre_à_canon = self.constructeur.get_object(
            "fabrique_de_poudre_à_canon")
        self.quartier_de_travailleurs = self.constructeur.get_object(
            "quartier_de_travailleurs")

        self.hangar_de_bois = self.constructeur.get_object("hangar_de_bois")
        self.hangar_de_métal = self.constructeur.get_object("hangar_de_métal")
        self.poudrière = self.constructeur.get_object("poudrière")
        self.chantier_naval = self.constructeur.get_object(
            "chantier_naval_bâtiment")

        self.camp_de_formation_de_travailleurs\
            = self.constructeur.get_object("camp_de_formation_de_travailleurs")

        self.taverne = self.constructeur.get_object("taverne")
        self.laboratoire_de_recherche = self.constructeur.get_object(
            "laboratoire_de_recherche")
        self.bâtiment_en_construction = self.constructeur.get_object(
            "bâtiment_en_construction")

    def récupérer_étiquettes_recherches(self):
        self.espionnage = self.constructeur.get_object("espionnage")
        self.canons = self.constructeur.get_object("canons")
        self.voilure = self.constructeur.get_object("voilure")
        self.coque = self.constructeur.get_object("coque")
        self.colonisation = self.constructeur.get_object("colonisation")
        self.formation_officiers = self.constructeur.get_object("formation_officiers")

        self.coût_espionnage = self.constructeur.get_object("coût_espionnage")
        self.coût_canons = self.constructeur.get_object("coût_canons")
        self.coût_voilure = self.constructeur.get_object("coût_voilure")
        self.coût_coque = self.constructeur.get_object("coût_coque")
        self.coût_colonisation = self.constructeur.get_object("coût_colonisation")
        self.coût_formation_officiers = self.constructeur.get_object("coût_formation_officiers")

    def afficher_bâtiments(self):
        if self.bâtiments is None:
            self.bâtiments = self.serveur.demander_niveau_bâtiments()

        bâtiments = self.bâtiments[self.île_sélectionnée - 1]

        self.forêt.set_text("Forêt (" + bâtiments[0] + ")")
        self.mine_de_métal.set_text("Mine de métal (" + bâtiments[1] + ")")
        self.fabrique_de_poudre_à_canon.set_text("Fabrique de poudre à canon ("
                                                 + bâtiments[2] + ")")
        self.hangar_de_bois.set_text("Hangar de bois (" + bâtiments[4] + ")")
        self.hangar_de_métal.set_text("Hangar de métal (" + bâtiments[5] + ")")
        self.poudrière.set_text("Poudrière (" + bâtiments[3] + ")")
        self.quartier_de_travailleurs.set_text("Quartier de travailleurs ("
                                               + bâtiments[6] + ")")
        self.chantier_naval.set_text("Chantier naval (" + bâtiments[7] + ")")
        self.laboratoire_de_recherche.set_text("Laboratoire de recherche ("
                                               + bâtiments[8] + ")")
        self.camp_de_formation_de_travailleurs.set_text(
            "Camp de formation de travailleurs (" + bâtiments[9] + ")")
        self.taverne.set_text("Taverne (" + bâtiments[10] + ")")

        self.serveur.y_a_t_il_une_construction_en_cours(self.île_sélectionnée)
        if self.serveur.construction_en_cours:
            self.bâtiment_en_construction.set_text(self.serveur.bâtiment_en_cours_de_construction
                                                   + ", fin de construction : "
                                                   + self.serveur.heure_fin_construction)
        else:
            self.bâtiment_en_construction.set_text("Il n’y a actuellement pas de construction "
                                                   + "en cours sur cette ile.")

        self.afficher_coût_bâtiments(bâtiments)
        self.afficher_production()

        return True

    def afficher_recherches(self):
        recherches = self.serveur.obtenir_niveau_recherches()
        if recherches is False:
            return False

        self.espionnage.set_text("Espionnage (" + recherches[0] + ")")
        self.canons.set_text("Canons (" + recherches[1] + ")")
        self.voilure.set_text("Voilure (" + recherches[2] + ")")
        self.coque.set_text("Coque (" + recherches[3] + ")")
        self.colonisation.set_text("Colonisation (" + recherches[4] + ")")
        self.formation_officiers.set_text("Formation d’officiers (" + recherches[5] + ")")

        self.afficher_coût_recherches(recherches)

        return True

    def afficher_coût_recherches(self, recherches):
        for i in range(len(recherches)):
            recherches[i] = int(recherches[i]) + 1

        nom_recherche = ["espionnage", "canons", "voilure", "coque",
                         "colonisation", "formation_officiers"]
        coût = []
        texte_coût = []
        for i in range(len(nom_recherche)):
            coût_recherche_courante = obtenir_coût_recherche(nom_recherche[i],
                                                             recherches[i])
            coût.append(coût_recherche_courante)
            texte = "Bois : " + str(coût_recherche_courante[0])
            texte += ", métal : " + str(coût_recherche_courante[1])
            texte += ", poudre à canon : " + str(coût_recherche_courante[2])
            texte_coût.append(texte)

        self.coût_espionnage.set_text(texte_coût[0])
        self.coût_canons.set_text(texte_coût[1])
        self.coût_voilure.set_text(texte_coût[2])
        self.coût_coque.set_text(texte_coût[3])
        self.coût_colonisation.set_text(texte_coût[4])
        self.coût_formation_officiers.set_text(texte_coût[5])

    def afficher_navires(self):
        navires = self.serveur.demander_nombre_navires(self.île_sélectionnée)

        self.bateau_espion.set_text("Bateau espion (" + navires[0] + ")")
        self.bateau_transport_léger.set_text("Bateau de transport léger ("
                                             + navires[1] + ")")
        self.bateau_transport_lourd.set_text("Bateau de transport lourd ("
                                             + navires[2] + ")")
        self.canonnière.set_text("Canonnière (" + navires[3] + ")")
        self.frégate.set_text("Frégate (" + navires[4] + ")")
        self.corvette.set_text("Corvette (" + navires[5] + ")")
        self.brick.set_text("Brick (" + navires[6] + ")")
        self.galion.set_text("Galion (" + navires[7] + ")")
        self.navire_de_ligne.set_text("Navire de ligne (" + navires[8] + ")")

        self.borner_ajustements_flotte(navires)

        return True

    def afficher_défenses(self):
        défenses = self.serveur.demander_nombre_défenses(self.île_sélectionnée)

        self.mur.set_text("Mur (" + défenses[0] + ")")
        self.canon.set_text("Canon (" + défenses[1] + ")")
        self.mine.set_text("Mine (" + défenses[2] + ")")
        self.mortier.set_text("Mortier (" + défenses[3] + ")")
        self.fort.set_text("Fort (" + défenses[4] + ")")

        return True

    def afficher_coût_bâtiments(self, niveau_bâtiments):
        coût_forêt = obtenir_coût_bâtiment("forêt",
                                           int(niveau_bâtiments[0]) + 1)
        temps_forêt = obtenir_temps_construction(coût_forêt,
                                                 int(niveau_bâtiments[9]),
                                                 int(niveau_bâtiments[10]))
        chaîne_coût_forêt = fabriquer_chaîne_ressources(coût_forêt,
                                                        temps_forêt)
        self.coût_forêt.set_text(chaîne_coût_forêt)

        coût_mine_de_métal = obtenir_coût_bâtiment("mine de métal",
                                                   int(niveau_bâtiments[1])
                                                   + 1)
        temps_mine_de_métal = obtenir_temps_construction(coût_mine_de_métal, int(niveau_bâtiments[9]),
                                                         int(niveau_bâtiments[10]))
        chaîne_coût_mine_de_métal = fabriquer_chaîne_ressources(
            coût_mine_de_métal, temps_mine_de_métal)
        self.coût_mine_de_métal.set_text(chaîne_coût_mine_de_métal)

        coût_fabrique_de_poudre_à_canon = obtenir_coût_bâtiment("fabrique de poudre à canon",
                                                                int(niveau_bâtiments[2]) + 1)
        temps_fabrique_de_poudre_à_canon = obtenir_temps_construction(coût_fabrique_de_poudre_à_canon,
                                                                      int(
                                                                          niveau_bâtiments[9]),
                                                                      int(niveau_bâtiments[10]))
        chaîne_coût_fabrique_de_poudre_à_canon = fabriquer_chaîne_ressources(
            coût_fabrique_de_poudre_à_canon, temps_fabrique_de_poudre_à_canon)
        self.coût_fabrique_de_poudre_à_canon.set_text(
            chaîne_coût_fabrique_de_poudre_à_canon)

        coût_quartier_de_travailleurs = obtenir_coût_bâtiment(
            "quartier de travailleurs", int(niveau_bâtiments[6]) + 1)
        temps_quartier_de_travailleurs = obtenir_temps_construction(
            coût_quartier_de_travailleurs, int(niveau_bâtiments[9]), int(niveau_bâtiments[10]))
        chaîne_coût_quartier_de_travailleurs = fabriquer_chaîne_ressources(
            coût_quartier_de_travailleurs, temps_quartier_de_travailleurs)
        self.coût_quartier_de_travailleurs.set_text(
            chaîne_coût_quartier_de_travailleurs)

        self.coût_hangar_de_bois.set_text(
            "Ce bâtiment n’est pas disponible pour le moment.")
        self.coût_hangar_de_métal.set_text(
            "Ce bâtiment n’est pas disponible pour le moment.")
        self.coût_poudrière.set_text(
            "Ce bâtiment n’est pas disponible pour le moment.")

        coût_chantier_naval = obtenir_coût_bâtiment(
            "chantier naval", int(niveau_bâtiments[7]) + 1)
        temps_chantier_naval = obtenir_temps_construction(
            coût_chantier_naval, int(niveau_bâtiments[9]), int(niveau_bâtiments[10]))
        chaîne_coût_chantier_naval = fabriquer_chaîne_ressources(
            coût_chantier_naval, temps_chantier_naval)
        self.coût_chantier_naval.set_text(chaîne_coût_chantier_naval)

        coût_laboratoire_de_recherche = obtenir_coût_bâtiment(
            "laboratoire de recherche", int(niveau_bâtiments[8]) + 1)
        temps_laboratoire_de_recherche = obtenir_temps_construction(
            coût_laboratoire_de_recherche, int(niveau_bâtiments[9]), int(niveau_bâtiments[10]))
        chaîne_coût_laboratoire_de_recherche = fabriquer_chaîne_ressources(
            coût_laboratoire_de_recherche, temps_laboratoire_de_recherche)
        self.coût_laboratoire_de_recherche.set_text(
            chaîne_coût_laboratoire_de_recherche)

        coût_camp_de_formation_de_travailleurs = obtenir_coût_bâtiment(
            "camp de formation de travailleurs", int(niveau_bâtiments[9]) + 1)
        temps_camp_de_travailleurs = obtenir_temps_construction(
            coût_camp_de_formation_de_travailleurs, int(niveau_bâtiments[9]), int(niveau_bâtiments[10]))
        chaîne_coût_camp_de_formation_de_travailleurs\
            = fabriquer_chaîne_ressources(coût_camp_de_formation_de_travailleurs,
                                          temps_camp_de_travailleurs)
        self.coût_camp_de_formation_de_travailleurs.set_text(
            chaîne_coût_camp_de_formation_de_travailleurs)

        coût_taverne = obtenir_coût_bâtiment(
            "taverne", int(niveau_bâtiments[10]) + 1)
        temps_taverne = obtenir_temps_construction(
            coût_taverne, int(niveau_bâtiments[9]), int(niveau_bâtiments[10]))
        chaîne_coût_taverne = fabriquer_chaîne_ressources(
            coût_taverne, temps_taverne)
        self.coût_taverne.set_text(chaîne_coût_taverne)

    def récupérer_étiquettes_coûts(self):
        self.coût_forêt = self.constructeur.get_object("coût_forêt")
        self.coût_mine_de_métal = self.constructeur.get_object(
            "coût_mine_de_métal")
        self.coût_fabrique_de_poudre_à_canon = self.constructeur.get_object(
            "coût_fabrique_de_poudre_à_canon")
        self.coût_quartier_de_travailleurs = self.constructeur.get_object(
            "coût_quartier_de_travailleurs")
        self.coût_hangar_de_bois = self.constructeur.get_object(
            "coût_hangar_de_bois")
        self.coût_hangar_de_métal = self.constructeur.get_object(
            "coût_hangar_de_métal")
        self.coût_poudrière = self.constructeur.get_object("coût_poudrière")
        self.coût_chantier_naval = self.constructeur.get_object(
            "coût_chantier_naval")
        self.coût_laboratoire_de_recherche = self.constructeur.get_object(
            "coût_laboratoire_de_recherche")
        self.coût_camp_de_formation_de_travailleurs = self.constructeur.get_object(
            "coût_camp_de_formation_de_travailleurs")
        self.coût_taverne = self.constructeur.get_object("coût_taverne")

    def récupérer_étiquettes_navires(self):
        self.bateau_espion = self.constructeur.get_object("bateau_espion")
        self.canonnière = self.constructeur.get_object("canonnière")
        self.frégate = self.constructeur.get_object("frégate")
        self.corvette = self.constructeur.get_object("corvette")
        self.brick = self.constructeur.get_object("brick")
        self.galion = self.constructeur.get_object("galion")
        self.navire_de_ligne = self.constructeur.get_object("navire_de_ligne")
        self.bateau_transport_léger = self.constructeur.get_object(
            "bateau_transport_léger")
        self.bateau_transport_lourd = self.constructeur.get_object(
            "bateau_transport_lourd")

        self.coût_bateau_espion = self.constructeur.get_object(
            "coût_bateau_espion")
        self.coût_bateau_transport_léger = self.constructeur.get_object(
            "coût_bateau_de_transport_léger")
        self.coût_bateau_transport_lourd = self.constructeur.get_object(
            "coût_bateau_de_transport_lourd")
        self.coût_canonnière = self.constructeur.get_object("coût_canonnière")
        self.coût_frégate = self.constructeur.get_object("coût_frégate")
        self.coût_corvette = self.constructeur.get_object("coût_corvette")
        self.coût_brick = self.constructeur.get_object("coût_brick")
        self.coût_galion = self.constructeur.get_object("coût_galion")
        self.coût_navire_de_ligne = self.constructeur.get_object(
            "coût_navire_de_ligne")

    def récupérer_bc_navires(self):
        self.bc_bateau_espion = self.constructeur.get_object(
            "bouton_compteur_bateau_espion")
        self.bc_canonnière = self.constructeur.get_object(
            "bouton_compteur_canonnière")
        self.bc_corvette = self.constructeur.get_object(
            "bouton_compteur_corvette")
        self.bc_frégate = self.constructeur.get_object(
            "bouton_compteur_frégate")
        self.bc_brick = self.constructeur.get_object("bouton_compteur_brick")
        self.bc_galion = self.constructeur.get_object("bouton_compteur_galion")
        self.bc_ndl = self.constructeur.get_object(
            "bouton_compteur_navire_de_ligne")
        self.bc_bt_léger = self.constructeur.get_object(
            "bouton_compteur_bateau_de_transport_léger")
        self.bc_bt_lourd = self.constructeur.get_object(
            "bouton_compteur_bateau_de_transport_lourd")

    def récupérer_bc_défenses(self):
        self.bc_mur = self.constructeur.get_object("bouton_compteur_mur")
        self.bc_canon = self.constructeur.get_object("bouton_compteur_canon")
        self.bc_mine = self.constructeur.get_object("bouton_compteur_mine")
        self.bc_mortier = self.constructeur.get_object(
            "bouton_compteur_mortier")
        self.bc_fort = self.constructeur.get_object("bouton_compteur_fort")

    def récupérer_bc_vue_océan(self):
        self.bc_océan = self.constructeur.get_object("bc_océan")
        self.bc_secteur = self.constructeur.get_object("bc_secteur")
        self.bc_océan.set_value(self.îles[self.île_sélectionnée - 1]["océan"])
        self.bc_secteur.set_value(
            self.îles[self.île_sélectionnée - 1]["secteur"])

    def récupérer_bc_vue_flotte(self):
        self.bc_flotte_espion = self.constructeur.get_object(
            "bc_flotte_espion")
        self.bc_flotte_tlé = self.constructeur.get_object("bc_flotte_tlé")
        self.bc_flotte_tlo = self.constructeur.get_object("bc_flotte_tlo")
        self.bc_flotte_canonnière = self.constructeur.get_object(
            "bc_flotte_canonnière")
        self.bc_flotte_frégate = self.constructeur.get_object(
            "bc_flotte_frégate")
        self.bc_flotte_corvette = self.constructeur.get_object(
            "bc_flotte_corvette")
        self.bc_flotte_brick = self.constructeur.get_object("bc_flotte_brick")
        self.bc_flotte_galion = self.constructeur.get_object(
            "bc_flotte_galion")
        self.bc_flotte_ndl = self.constructeur.get_object("bc_flotte_ndl")
        self.bc_flotte_océan = self.constructeur.get_object("bc_flotte_océan")
        self.bc_flotte_secteur = self.constructeur.get_object(
            "bc_flotte_secteur")
        self.bc_flotte_emplacement = self.constructeur.get_object(
            "bc_flotte_emplacement")
        self.bc_flotte_bois = self.constructeur.get_object("bc_flotte_bois")
        self.bc_flotte_métal = self.constructeur.get_object("bc_flotte_métal")
        self.bc_flotte_pàc = self.constructeur.get_object("bc_flotte_pàc")

    def récupérer_bc(self):
        self.récupérer_bc_navires()
        self.récupérer_bc_défenses()
        self.récupérer_bc_vue_océan()
        self.récupérer_bc_vue_flotte()
        self.bc_message = self.constructeur.get_object("numéro_message")

    def récupérer_étiquettes_défenses(self):
        self.mur = self.constructeur.get_object("mur")
        self.canon = self.constructeur.get_object("canon")
        self.mine = self.constructeur.get_object("mine")
        self.mortier = self.constructeur.get_object("mortier")
        self.fort = self.constructeur.get_object("fort")

        self.coût_mur = self.constructeur.get_object("coût_mur")
        self.coût_canon = self.constructeur.get_object("coût_canon")
        self.coût_mine = self.constructeur.get_object("coût_mine")
        self.coût_mortier = self.constructeur.get_object("coût_mortier")
        self.coût_fort = self.constructeur.get_object("coût_fort")

    def récupérer_étiquettes_îles(self):
        self.île1_nom = self.constructeur.get_object("nom_de_l’île_1")
        self.île2_nom = self.constructeur.get_object("nom_de_l’île_2")
        self.île3_nom = self.constructeur.get_object("nom_de_l’île_3")
        self.île4_nom = self.constructeur.get_object("nom_de_l’île_4")
        self.île5_nom = self.constructeur.get_object("nom_de_l’île_5")
        self.île6_nom = self.constructeur.get_object("nom_de_l’île_6")
        self.île7_nom = self.constructeur.get_object("nom_de_l’île_7")
        self.île8_nom = self.constructeur.get_object("nom_de_l’île_8")
        self.île9_nom = self.constructeur.get_object("nom_de_l’île_9")
        self.île10_nom = self.constructeur.get_object("nom_de_l’île_10")
        self.île11_nom = self.constructeur.get_object("nom_de_l’île_11")
        self.île12_nom = self.constructeur.get_object("nom_de_l’île_12")
        self.île13_nom = self.constructeur.get_object("nom_de_l’île_13")
        self.île14_nom = self.constructeur.get_object("nom_de_l’île_14")
        self.île15_nom = self.constructeur.get_object("nom_de_l’île_15")

        self.île1_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_1")
        self.île2_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_2")
        self.île3_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_3")
        self.île4_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_4")
        self.île5_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_5")
        self.île6_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_6")
        self.île7_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_7")
        self.île8_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_8")
        self.île9_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_9")
        self.île10_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_10")
        self.île11_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_11")
        self.île12_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_12")
        self.île13_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_13")
        self.île14_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_14")
        self.île15_propriétaire = self.constructeur.get_object(
            "nom_du_joueur_15")

    def fenêtre_supprimé(self, *args):
        Gtk.main_quit(*args)

    def améliorer_la_forêt(self, bouton):
        self.serveur.construire_bâtiment(self.île_sélectionnée, "forêt")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_la_mine_de_métal(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "mine_de_métal")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_la_fabrique_de_poudre_à_canon(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "fabrique_poudre_canon")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_le_quartier_de_travailleurs(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "quartier_travailleurs")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_le_chantier_naval(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "chantier_naval")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_le_camp_de_formation_de_travailleurs(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "camp_formation_travailleurs")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_la_taverne(self, bouton):
        self.serveur.construire_bâtiment(self.île_sélectionnée, "taverne")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def améliorer_le_laboratoire_de_recherche(self, bouton):
        self.serveur.construire_bâtiment(
            self.île_sélectionnée, "laboratoire_recherche")
        self.afficher_bâtiments()
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_bateau_espion(self, bouton):
        nombre = self.bc_bateau_espion.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "bateau_espion", nombre)
        self.bc_bateau_espion.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_canonnière(self, bouton):
        nombre = self.bc_canonnière.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "canonnière", nombre)
        self.bc_canonnière.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_frégate(self, bouton):
        nombre = self.bc_frégate.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "frégate", nombre)
        self.bc_frégate.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_corvette(self, bouton):
        nombre = self.bc_corvette.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "corvette", nombre)
        self.bc_corvette.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_brick(self, bouton):
        nombre = self.bc_brick.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "brick", nombre)
        self.bc_brick.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_galion(self, bouton):
        nombre = self.bc_galion.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "galion", nombre)
        self.bc_galion.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_navire_de_ligne(self, bouton):
        nombre = self.bc_ndl.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "navire_de_ligne", nombre)
        self.bc_navire_de_ligne.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_bt_léger(self, bouton):
        nombre = self.bc_bt_léger.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "bateau_transport_léger", nombre)
        self.bc_bt_léger.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_bt_lourd(self, bouton):
        nombre = self.bc_bt_lourd.get_value_as_int()
        self.serveur.construire_navires(self.île_sélectionnée,
                                        "bateau_transport_lourd", nombre)
        self.bc_bt_lourd.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_mine(self, bouton):
        nombre = self.bc_mine.get_value_as_int()
        self.serveur.construire_défenses(self.île_sélectionnée,
                                         "mine", nombre)
        self.afficher_ressources()
        self.bc_mine.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_canon(self, bouton):
        nombre = self.bc_canon.get_value_as_int()
        self.serveur.construire_défenses(self.île_sélectionnée,
                                         "canon", nombre)
        self.afficher_ressources()
        self.bc_canon.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_mur(self, bouton):
        nombre = self.bc_mur.get_value_as_int()
        self.serveur.construire_défenses(self.île_sélectionnée,
                                         "mur", nombre)
        self.afficher_ressources()
        self.bc_mur.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_mortier(self, bouton):
        nombre = self.bc_mortier.get_value_as_int()
        self.serveur.construire_défenses(self.île_sélectionnée,
                                         "mortier", nombre)
        self.afficher_ressources()
        self.bc_mortier.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def c_fort(self, bouton):
        nombre = self.bc_fort.get_value_as_int()
        self.serveur.construire_défenses(self.île_sélectionnée,
                                         "fort", nombre)
        self.afficher_ressources()
        self.bc_fort.set_value(0)
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_espionnage(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 3:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau trois."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "espionnage")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_canons(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 2:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau deux."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "canons")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_voilure(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 2:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau deux."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "voilure")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_coque(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 2:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau deux."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "coque")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_colonisation(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 5:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau cinq."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "colonisation")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def rechercher_formation_officiers(self, bouton=None):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        if int(bâtiments[8]) < 1:
            message = "Le laboratoire de recherche doit être au moins au "
            message += "niveau un."
            self.fenêtre_surgissante(message=message)
        self.serveur.rechercher(self.île_sélectionnée, "formation_officiers")
        self.afficher_ressources()
        self.mettre_à_jour_constructions_en_cours(forcer=True)
        self.afficher_constructions_en_cours()
        self.màj_constructions()

    def afficher_vue_océan(self, bc=0):
        océan = self.bc_océan.get_value_as_int()
        secteur = self.bc_secteur.get_value_as_int()

        vue = self.serveur.demander_informations_secteur(océan, secteur)

        for i in range(15):
            if vue[i][0] == "LIBRE":
                vue[i][0] = "île déserte"

            if vue[i][1] == "LIBRE":
                vue[i][1] = ""

        self.île1_nom.set_text(vue[0][0])
        self.île1_propriétaire.set_text(vue[0][1])
        self.île2_nom.set_text(vue[1][0])
        self.île2_propriétaire.set_text(vue[1][1])
        self.île3_nom.set_text(vue[2][0])
        self.île3_propriétaire.set_text(vue[2][1])
        self.île4_nom.set_text(vue[3][0])
        self.île4_propriétaire.set_text(vue[3][1])
        self.île5_nom.set_text(vue[4][0])
        self.île5_propriétaire.set_text(vue[4][1])
        self.île6_nom.set_text(vue[5][0])
        self.île6_propriétaire.set_text(vue[5][1])
        self.île7_nom.set_text(vue[6][0])
        self.île7_propriétaire.set_text(vue[6][1])
        self.île8_nom.set_text(vue[7][0])
        self.île8_propriétaire.set_text(vue[7][1])
        self.île9_nom.set_text(vue[8][0])
        self.île9_propriétaire.set_text(vue[8][1])
        self.île10_nom.set_text(vue[9][0])
        self.île10_propriétaire.set_text(vue[9][1])
        self.île11_nom.set_text(vue[10][0])
        self.île11_propriétaire.set_text(vue[10][1])
        self.île12_nom.set_text(vue[11][0])
        self.île12_propriétaire.set_text(vue[11][1])
        self.île13_nom.set_text(vue[12][0])
        self.île13_propriétaire.set_text(vue[12][1])
        self.île14_nom.set_text(vue[13][0])
        self.île14_propriétaire.set_text(vue[13][1])
        self.île15_nom.set_text(vue[14][0])
        self.île15_propriétaire.set_text(vue[14][1])

    def passer_à_la_vue_flotte(self, emplacement):
        numéro_emplacement = int(emplacement.get_text())
        océan = self.bc_océan.get_value_as_int()
        secteur = self.bc_secteur.get_value_as_int()
        self.bloc_notes.set_current_page(9)
        self.bc_flotte_océan.set_value(océan)
        self.bc_flotte_secteur.set_value(secteur)
        self.bc_flotte_emplacement.set_value(numéro_emplacement)

    def afficher_message(self, bc=0):
        numéro_message = self.bc_message.get_value_as_int()
        message = self.message.obtenir_message(numéro_message)
        if message is not None:
            self.tampon_message.set_text(message)

    def borner_ajustements_flotte(self, navires=None):
        if navires != None:
            self.ajustement_bc_flotte_espion.set_lower(0)
            self.ajustement_bc_flotte_espion.set_upper(int(navires[0]))
            self.ajustement_bc_flotte_tlé.set_lower(0)
            self.ajustement_bc_flotte_tlé.set_upper(int(navires[1]))
            self.ajustement_bc_flotte_tlo.set_lower(0)
            self.ajustement_bc_flotte_tlo.set_upper(int(navires[2]))
            self.ajustement_bc_flotte_canonnière.set_lower(0)
            self.ajustement_bc_flotte_canonnière.set_upper(int(navires[3]))
            self.ajustement_bc_flotte_frégate.set_lower(0)
            self.ajustement_bc_flotte_frégate.set_upper(int(navires[4]))
            self.ajustement_bc_flotte_corvette.set_lower(0)
            self.ajustement_bc_flotte_corvette.set_upper(int(navires[5]))
            self.ajustement_bc_flotte_brick.set_lower(0)
            self.ajustement_bc_flotte_brick.set_upper(int(navires[6]))
            self.ajustement_bc_flotte_galion.set_lower(0)
            self.ajustement_bc_flotte_galion.set_upper(int(navires[7]))
            self.ajustement_bc_flotte_ndl.set_lower(0)
            self.ajustement_bc_flotte_ndl.set_upper(int(navires[8]))

        self.ajustement_bc_flotte_bois.set_lower(0)
        self.ajustement_bc_flotte_bois.set_upper(int(self.ressources[0]))
        self.ajustement_bc_flotte_métal.set_lower(0)
        self.ajustement_bc_flotte_métal.set_upper(int(self.ressources[1]))
        self.ajustement_bc_flotte_pàc.set_lower(0)
        self.ajustement_bc_flotte_pàc.set_upper(int(self.ressources[2]))

    def envoyer_flotte(self, bouton):
        # On récupère le nombre de navires.
        flotte = []
        flotte.append(self.bc_flotte_espion.get_value_as_int())
        flotte.append(self.bc_flotte_tlé.get_value_as_int())
        flotte.append(self.bc_flotte_tlo.get_value_as_int())
        flotte.append(self.bc_flotte_canonnière.get_value_as_int())
        flotte.append(self.bc_flotte_corvette.get_value_as_int())
        flotte.append(self.bc_flotte_frégate.get_value_as_int())
        flotte.append(self.bc_flotte_brick.get_value_as_int())
        flotte.append(self.bc_flotte_galion.get_value_as_int())
        flotte.append(self.bc_flotte_ndl.get_value_as_int())

        # S’il n’y a aucun navires, on ne fait rien.
        aucun_navire = True
        for i in flotte:
            if i > 0:
                aucun_navire = False
                break
        if aucun_navire:
            # Il faut afficher qu’aucun navire n’a été sélectionné.
            self.fenêtre_surgissante\
                (message="Cette flotte ne contient aucun navire.")
            return

        # On récupère les ressources.
        ressources = {}
        ressources["bois"] = self.bc_flotte_bois.get_value_as_int()
        ressources["métal"] = self.bc_flotte_métal.get_value_as_int()
        ressources["pàc"] = self.bc_flotte_pàc.get_value_as_int()

        # On récupère le type de mission.
        itérateur_type_mission = self.type_mission.get_active_iter()
        if itérateur_type_mission is None:
            return
        type_mission\
            = self.modèle_type_mission.get_value(itérateur_type_mission, 0)

        # On récupère les coordonnées
        océan = self.bc_flotte_océan.get_value_as_int()
        secteur = self.bc_flotte_secteur.get_value_as_int()
        emplacement = self.bc_flotte_emplacement.get_value_as_int()

        numéro_île_arrivée = 0
        if type_mission == "Stationner":
            for i in range(len(self.îles)):
                île = self.îles[i]
                if île["océan"] == océan and île["secteur"] == secteur\
                   and île["emplacement"] == emplacement:
                    numéro_île_arrivée = i + 1
            if numéro_île_arrivée == 0:
                message = "Vous ne pouvez stationner sur une île ne vous appartenant pas."
                self.fenêtre_surgissante("Erreur.", "Réessayer", message)
                return

        # On lance l’expédition
        réussi = self.serveur.lancer_expédition(flotte, self.île_sélectionnée,
                                                type_mission, numéro_île_arrivée,
                                                océan, secteur, emplacement,
                                                ressources)

        # On réaffiche les navires
        self.afficher_navires()

        try:
            if réussi:
                self.ràz_flotte(bouton)
                self.expéditions.rafraîchir()
        except:
            self.fenêtre_surgissante ("Erreur.", "Réessayer", réussi)

    def ràz_flotte(self, bouton):
        self.bc_flotte_espion.set_value(0)
        self.bc_flotte_tlé.set_value(0)
        self.bc_flotte_tlo.set_value(0)
        self.bc_flotte_canonnière.set_value(0)
        self.bc_flotte_frégate.set_value(0)
        self.bc_flotte_corvette.set_value(0)
        self.bc_flotte_brick.set_value(0)
        self.bc_flotte_galion.set_value(0)
        self.bc_flotte_ndl.set_value(0)
        self.bc_flotte_océan.set_value(0)
        self.bc_flotte_secteur.set_value(0)
        self.bc_flotte_emplacement.set_value(0)
        self.bc_flotte_bois.set_value(0)
        self.bc_flotte_métal.set_value(0)
        self.bc_flotte_pàc.set_value(0)

    def afficher_production(self):
        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        self.production_bois.set_text(str(obtenir_production_bois
                                          (int(bâtiments[0]))))
        self.production_métal.set_text(str(obtenir_production_métal
                                           (int(bâtiments[1]))))
        self.production_pàc.set_text(str(obtenir_production_pàc
                                         (int(bâtiments[2]))))
        self.production_travailleurs.set_text(str(obtenir_production_travailleurs
                                                  (int(bâtiments[6]))))

        self.production_bois_niveau_suivant.set_text(str(obtenir_production_bois
                                                         (int(bâtiments[0]) + 1)))
        self.production_métal_niveau_suivant.set_text(str(obtenir_production_métal
                                                          (int(bâtiments[1]) + 1)))
        self.production_pàc_niveau_suivant.set_text(str(obtenir_production_pàc
                                                        (int(bâtiments[2]) + 1)))
        self.production_travailleurs_niveau_suivant.set_text(
            str(obtenir_production_travailleurs(int(bâtiments[6]) + 1)))

    def remplir_modèle_liste_île(self):
        self.modèle_liste_île = self.constructeur.get_object(
            "modèle_liste_île")
        self.liste_île = self.constructeur.get_object("séléction_ile")
        for i in range(len(self.îles)):
            self.modèle_liste_île.append([i, self.îles[i]["nom"]])
        self.liste_île.set_active(0)

    def changement_île(self, b):
        itérateur_île_sélectionnée = self.liste_île.get_active_iter()
        if itérateur_île_sélectionnée is None:
            return
        self.île_sélectionnée = self.modèle_liste_île.get_value(
            itérateur_île_sélectionnée, 0) + 1
        self.afficher_ressources()
        self.afficher_bâtiments()
        self.afficher_navires()
        self.afficher_défenses()
        self.bc_océan.set_value(self.îles[self.île_sélectionnée - 1]["océan"])
        self.bc_secteur.set_value(
            self.îles[self.île_sélectionnée - 1]["secteur"])
        self.afficher_vue_océan()
        # self.mettre_à_jour_constructions_en_cours (forcer=True)
        self.afficher_constructions_en_cours()
        self.afficher_informations_sur_île()

    def afficher_coût_production_navires(self, bc=0):
        nombre_navires = []
        nombre_navires.append(self.bc_bateau_espion.get_value_as_int())
        nombre_navires.append(self.bc_canonnière.get_value_as_int())
        nombre_navires.append(self.bc_corvette.get_value_as_int())
        nombre_navires.append(self.bc_frégate.get_value_as_int())
        nombre_navires.append(self.bc_brick.get_value_as_int())
        nombre_navires.append(self.bc_galion.get_value_as_int())
        nombre_navires.append(self.bc_ndl.get_value_as_int())
        nombre_navires.append(self.bc_bt_léger.get_value_as_int())
        nombre_navires.append(self.bc_bt_lourd.get_value_as_int())

        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        niveau_chantier_naval = int(bâtiments[7])
        niveau_taverne = int(bâtiments[10])

        coût_navires = []
        coût_navires.append([0, 1000, 0])
        coût_navires.append([3000, 1000, 0])
        coût_navires.append([6000, 4000, 0])
        coût_navires.append([20000, 7000, 2000])
        coût_navires.append([45000, 15000, 0])
        coût_navires.append([60000, 50000, 15000])
        coût_navires.append([5000000, 4000000, 1000000])
        coût_navires.append([2000, 2000, 0])
        coût_navires.append([6000, 6000, 0])

        temps_navires = []
        for coût_navire in coût_navires:
            somme_coût_navire = coût_navire[0] + \
                coût_navire[1] + coût_navire[2]
            temps_navire = somme_coût_navire / \
                (5000 * (niveau_chantier_naval + 1)) * 3600
            temps_navire /= 2 ** niveau_taverne
            temps_navire = int(temps_navire)
            temps_navires.append(temps_navire)

        coût_temps_navires = []
        for i in range(len(nombre_navires)):
            coût_temps_navires.append([])
            for j in range(len(coût_navires[i])):
                coût_temps_navires[i].append(
                    coût_navires[i][j] * nombre_navires[i])
            coût_temps_navires[i].append(temps_navires[i] * nombre_navires[i])

        chaines = []
        for i in range(len(coût_temps_navires)):
            chaine = "Bois : " + str(coût_temps_navires[i][0])
            chaine += " Métal : " + str(coût_temps_navires[i][1])
            chaine += " Poudre à canon : " + str(coût_temps_navires[i][2])
            chaine += " Temps : " + str(coût_temps_navires[i][3]) + "s"
            chaines.append(chaine)

        self.coût_bateau_espion.set_text(chaines[0])
        self.coût_canonnière.set_text(chaines[1])
        self.coût_corvette.set_text(chaines[2])
        self.coût_frégate.set_text(chaines[3])
        self.coût_brick.set_text(chaines[4])
        self.coût_galion.set_text(chaines[5])
        self.coût_navire_de_ligne.set_text(chaines[6])
        self.coût_bateau_transport_léger.set_text(chaines[7])
        self.coût_bateau_transport_lourd.set_text(chaines[8])

    def afficher_coût_production_défenses(self, bc=0):
        nombre_défenses = []
        nombre_défenses.append(self.bc_mur.get_value_as_int())
        nombre_défenses.append(self.bc_canon.get_value_as_int())
        nombre_défenses.append(self.bc_mine.get_value_as_int())
        nombre_défenses.append(self.bc_mortier.get_value_as_int())
        nombre_défenses.append(self.bc_fort.get_value_as_int())

        bâtiments = self.bâtiments[self.île_sélectionnée - 1]
        niveau_chantier_naval = int(bâtiments[7])
        niveau_taverne = int(bâtiments[10])

        coût_défenses = []
        coût_défenses.append([7500, 5000, 0])
        coût_défenses.append([10000, 7500, 3000])
        coût_défenses.append([5000, 2000, 1000])
        coût_défenses.append([20000, 15000, 6000])
        coût_défenses.append([200000, 135000, 100000])

        temps_défenses = []
        for coût_défense in coût_défenses:
            somme_coût_défense = coût_défense[0] + \
                coût_défense[1] + coût_défense[2]
            temps_défense = somme_coût_défense / \
                (5000 * (niveau_chantier_naval + 1)) * 3600
            temps_défense /= 2 ** niveau_taverne
            temps_défense = int(temps_défense)
            temps_défenses.append(temps_défense)

        coût_temps_défenses = []
        for i in range(len(nombre_défenses)):
            coût_temps_défenses.append([])
            for j in range(len(coût_défenses[i])):
                coût_temps_défenses[i].append(
                    coût_défenses[i][j] * nombre_défenses[i])
            coût_temps_défenses[i].append(
                temps_défenses[i] * nombre_défenses[i])

        chaines = []
        for i in range(len(coût_temps_défenses)):
            chaine = "Bois : " + str(coût_temps_défenses[i][0])
            chaine += " Métal : " + str(coût_temps_défenses[i][1])
            chaine += " Poudre à canon : " + str(coût_temps_défenses[i][2])
            chaine += " Temps : " + str(coût_temps_défenses[i][3]) + "s"
            chaines.append(chaine)

        self.coût_mur.set_text(chaines[0])
        self.coût_canon.set_text(chaines[1])
        self.coût_mine.set_text(chaines[2])
        self.coût_mortier.set_text(chaines[3])
        self.coût_fort.set_text(chaines[4])

    def mettre_à_jour_constructions_en_cours(self, b=0, forcer=False):
        try:
            if forcer is False and self.constructions_en_cours != []:
                return
        except:
            pass

        self.constructions_en_cours = []
        self.constructions_en_cours.append\
            (self.serveur.y_a_t_il_une_recherche_en_cours())
        for i in range(self.nombre_îles):
            construction_bâtiment_en_cours\
                = self.serveur.y_a_t_il_une_construction_en_cours(i + 1)
            construction_navires_en_cours\
                = self.serveur.y_a_t_il_une_construction_de_navires_en_cours(i + 1)
            construction_défenses_en_cours\
                = self.serveur.y_a_t_il_une_construction_de_défenses_en_cours(i + 1)
            self.constructions_en_cours.append([construction_bâtiment_en_cours,
                                                construction_navires_en_cours,
                                                construction_défenses_en_cours])


    def afficher_constructions_en_cours(self):
        texte = ""

        if self.constructions_en_cours[self.île_sélectionnée][0] is not False:
            texte += self.constructions_en_cours[self.île_sélectionnée][0][0]\
                         .capitalize()
            texte += " est en cours de construction.\n"
            texte += "La construction se terminera "
            texte += self.constructions_en_cours[self.île_sélectionnée][0][1]
            texte += ".\n\n"

        if self.constructions_en_cours[self.île_sélectionnée][1] is not False:
            nombre = self.constructions_en_cours[self.île_sélectionnée][1][1]
            texte += nombre + " "
            texte += self.constructions_en_cours[self.île_sélectionnée][1][0]
            if int(nombre) == 1:
                texte += " est en cours de construction.\n"
            else:
                texte += " sont en cours de construction.\n"
            texte += "La construction se terminera "
            texte += self.constructions_en_cours[self.île_sélectionnée][1][2]
            texte += ".\n\n"

        if self.constructions_en_cours[self.île_sélectionnée][2] is not False:
            nombre = self.constructions_en_cours[self.île_sélectionnée][2][1]
            texte += nombre + " "
            texte += self.constructions_en_cours[self.île_sélectionnée][2][0]
            if int(nombre) == 1:
                texte += " est en cours de construction.\n"
            else:
                texte += " sont en cours de construction.\n"
            texte += "La construction se terminera "
            texte += self.constructions_en_cours[self.île_sélectionnée][2][2]
            texte += ".\n\n"

        if self.constructions_en_cours[0] is not False:
            texte += self.constructions_en_cours[0][0].capitalize()
            texte += " est en cours de recherche.\n"
            texte += "La recherche se terminera "
            texte += self.constructions_en_cours[0][1] + "."

        self.étiquette_constructions_en_cours.set_text(texte)
        return False

    def afficher_informations_sur_île(self):
        île_actuelle = self.îles[self.île_sélectionnée - 1]

        texte = "Nom : " + île_actuelle["nom"] + "\n"
        texte += "Coordonnées : (" + str(île_actuelle["océan"]) + " ; "
        texte += str(île_actuelle["secteur"]) + " ; "
        texte += str(île_actuelle["emplacement"]) + ")\n"

        self.informations_îles.set_text(texte)

    def obtenir_constructions_en_cours(self):
        self.constructions = self.serveur.obtenir_constructions_en_cours()

    def augmenter_valeur_bâtiment(self, île, bâtiment):
        case = None
        if bâtiment == "forêt":
            case = 0
        elif bâtiment == "mine de métal":
            case = 1
        elif bâtiment == "fabrique de poudre à canon":
            case = 2
        elif bâtiment == "hangar de bois":
            case = 3
        elif bâtiment == "hangar de métal":
            case = 4
        elif bâtiment == "hangar de poudre à canon":
            case = 5
        elif bâtiment == "quartier de travailleurs":
            case = 6
        elif bâtiment == "chantier naval":
            case = 7
        elif bâtiment == "laboratoire de recherche":
            case = 8
        elif bâtiment == "camp de formation de travailleurs":
            case = 9
        elif bâtiment == "taverne":
            case = 10

        self.bâtiments[île][case] = str(int(self.bâtiments[île][case]) + 1)
        self.afficher_bâtiments()

    def màj_constructions(self):
        maintenant = time.time()
        temps = None

        for i in range(len(self.constructions)):
            construction = self.constructions[i][0]
            if construction is False:
                continue
            elif construction[1] <= maintenant:
                self.augmenter_valeur_bâtiment(i, construction[0])
            elif temps is None:
                temps = construction[1] + 1 - maintenant
            elif temps is not None and temps > (construction[1] - maintenant):
                temps = construction[1] + 1 - maintenant

        for i in range(len(self.constructions)):
            île = self.constructions[i]
            for j in range(1, 3):
                construction = île[j]
                if construction is False:
                    continue
                elif construction[2] <= maintenant:
                    self.afficher_navires()
                    self.afficher_défenses()
                    continue
                elif temps is None:
                    temps = construction[2] + 1 - maintenant
                elif temps is not None and temps > (construction[2] - maintenant):
                    temps = construction[2] + 1 - maintenant

        if temps is not None and temps > 0:
            GObject.timeout_add_seconds(temps, self.màj_constructions)

        return False

    def fenêtre_surgissante(self, titre="Erreur.", texte_bouton="Réessayer",
                            message=""):
        fenêtre_avertissement = Gtk.Dialog(titre, self.fenêtre)
        fenêtre_avertissement.add_button(texte_bouton, 1)

        conteneur = fenêtre_avertissement.get_content_area()
        conteneur.add(Gtk.Label(message))

        fenêtre_avertissement.show_all()
        fenêtre_avertissement.run()
        fenêtre_avertissement.destroy()


identifiants = obtenir_identifiants(adresse_serveur)
fen = Fenêtre(identifiants[0], identifiants[1])
