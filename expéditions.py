"""
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

La gestion de l’affichage des expéditions.
"""

import réseau

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject

import time


class Expéditions:
    def __init__(self, bloc_notes, serveur, pseudo):
        self.bloc_notes = bloc_notes
        self.serveur = serveur
        self.pseudo = pseudo
        self.rafraîchir()

    def rafraîchir(self):
        expéditions = self.serveur.obtenir_expéditions()
        liste = Gtk.ListBox()
        liste.set_selection_mode(Gtk.SelectionMode.NONE)

        self.bloc_notes.remove_page(10)

        if expéditions is False:
            self.bloc_notes.insert_page(liste, Gtk.Label("Expéditions"),
                                        10)
            return

        for expédition in expéditions:
            ligne = Gtk.ListBoxRow()

            texte = self.convertir_expédition_en_texte(expédition)
            étiquette = Gtk.Label(texte)
            étiquette.set_use_markup(True)

            ligne.add(étiquette)
            liste.insert(ligne, -1)

        liste.show_all()
        self.bloc_notes.insert_page(liste, Gtk.Label("Expéditions"),
                                    10)

    def convertir_expédition_en_texte(self, expédition):
        texte = ""

        # À enlever !
        hostile = False
        try:
            hostile = (expédition["cible"] == self.pseudo)
        except:
            pass

        hostile = hostile and (expédition["mission"] == "attaque"
                               or expédition["mission"] == "espionnage")

        if hostile:
            texte += '<span foreground="red">'

        texte += "<b>" + expédition["mission"].capitalize() + "</b> "

        # Départ.
        île_départ = expédition["île de départ"]
        texte += "depuis (" + str(île_départ["océan"]) + ", "
        texte += str(île_départ["secteur"]) + ", "
        texte += str(île_départ["emplacement"]) + ") "

        # Arrivée
        île_arrivée = expédition["île d’arrivée"]
        texte += "vers (" + str(île_arrivée["océan"]) + ", "
        texte += str(île_arrivée["secteur"]) + ", "
        texte += str(île_arrivée["emplacement"]) + ").\n"

        # Heure arrivée.
        date = expédition["fin"]["$date"] / 1000
        date_formatée = time.strftime("%A %d %B %Y à %X",
                                      time.localtime(date))
        texte += "Arrivée prévue le " + date_formatée + "."

        if hostile:
            texte += "</span>"

        return texte
