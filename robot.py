"""
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est un robot pour Nassau.
"""

import threading
import time
import math

import réseau
from ressources import *

serveur = ("s2.xn--vendmiaire-e7a.fr", 4284)


def lire_identifiants():
    identifiants = []

    try:
        fichier = open("identifiants", "r")

        for ligne in fichier:
            ligne = ligne.rstrip().split()
            identifiants.append(ligne)

        return identifiants
    except:
        print("Une erreur est survenue lors de ",
              "la lecture du fichier « identifiants ».")


def obtenir_comptes(identifiants):
    comptes = []
    for identifiant in identifiants:
        comptes.append(réseau.Serveur(serveur,
                                      identifiant[0],
                                      identifiant[1]))
    return comptes


def convertir_liste_chaînes_vers_entiers(liste):
    liste_intermédiaire = []
    for élément in liste:
        liste_intermédiaire.append(int(élément))
    return liste_intermédiaire


def obtenir_ressources_manquantes(ressources, coût):
    ressources_manquantes = []
    for i in range(len(ressources)):
        ressource_manquante = coût[i] - ressources[i]
        if ressource_manquante <= 0:
            ressource_manquante = 0
        ressources_manquantes.append(ressource_manquante)
    return ressources_manquantes


def obtenir_code_réseau(nom):
    if nom == "forêt":
        return "forêt"
    elif nom == "mine de métal":
        return "mine_de_métal"
    elif nom == "fabrique de poudre à canon":
        return "fabrique_poudre_canon"
    elif nom == "quartier de travailleurs":
        return "quartier_travailleurs"
    elif nom == "chantier naval":
        return "chantier_naval"
    elif nom == "laboratoire de recherche":
        return "laboratoire_recherche"
    elif nom == "camp de formation de travailleurs":
        return "camp_formation_travailleurs"
    elif nom == "taverne":
        return "taverne"
    else:
        return ""


def bâtiment_à_construire(compte, île, bâtiments):
    bâtiment_à_construire = ""
    niveau = 0

    if bâtiments[8] < math.floor(bâtiments[2] / 2):
        bâtiment_à_construire = "laboratoire de recherche"
        niveau = bâtiments[8] + 1
    elif bâtiments[9] < math.floor(bâtiments[0] / 2)\
       and bâtiments[2] > 1:
        bâtiment_à_construire = "camp de formation de travailleurs"
        niveau = bâtiments[9] + 1
    elif (bâtiments[0] - bâtiments[1]) < 2:
        bâtiment_à_construire = "forêt"
        niveau = bâtiments[0] + 1
    elif (bâtiments[1] - bâtiments[2]) < 2:
        bâtiment_à_construire = "mine de métal"
        niveau = bâtiments[1] + 1
    else:
        bâtiment_à_construire = "fabrique de poudre à canon"
        niveau = bâtiments[2] + 1

    ressources = compte.récupérer_ressources(île)
    ressources = convertir_liste_chaînes_vers_entiers(ressources)
    coût = obtenir_coût_bâtiment(bâtiment_à_construire, niveau)
    if ressources[3] < coût[3]:
        bâtiment_à_construire = "quartier de travailleurs"
        niveau = bâtiments[6] + 1

    return [bâtiment_à_construire, niveau]


def construire_bâtiments(compte, bâtiments):
    constructions = compte.obtenir_constructions_en_cours()
    temps_attente = []

    for i in range(1, compte.nombre_îles + 1):
        if constructions[i - 1][0] is not False:
            temps = math.ceil(constructions[i - 1][0][1] - time.time()) + 1
            temps_attente.append(temps)
            continue

        bâtiment = bâtiment_à_construire(compte, i, bâtiments[i - 1])
        ressources = convertir_liste_chaînes_vers_entiers(
            compte.récupérer_ressources(i))
        coût = obtenir_coût_bâtiment(bâtiment[0], bâtiment[1])
        ressources_manquantes = obtenir_ressources_manquantes(ressources,
                                                              coût)

        temps_production = obtenir_temps_production(ressources_manquantes,
                                                    bâtiments[i - 1])
        if temps_production > 0:
            print("En attente de", temps_production,
                  "seconde(s) pour la production de ressources\nchez",
                  compte.pseudo, "en vue de construire " + bâtiment[0] + ".\n")
            temps_attente.append(temps_production)
            continue

        print("On construit", bâtiment[0], "au niveau", bâtiment[1], "pour",
              compte.pseudo + ".")
        compte.construire_bâtiment(i, obtenir_code_réseau(bâtiment[0]))
        temps_construction = obtenir_temps_construction(coût,
                                                        bâtiments[i - 1][9],
                                                        bâtiments[i - 1][10])
        temps_attente.append(temps_construction)

    temps_attente.sort()
    return temps_attente[0]


def lancer_robot(compte, essai):
    temps_attente = 0
    while True:
        if temps_attente > 0:
            time.sleep(temps_attente)

        bâtiments = compte.demander_niveau_bâtiments()
        for i in range(len(bâtiments)):
            bâtiments[i] = convertir_liste_chaînes_vers_entiers(bâtiments[i])

        temps_attente = construire_bâtiments(compte, bâtiments)
        # if temps_attente > 0:
        #     print("Il faut attendre", temps_attente, "secondes.")
        # else:
        #     print("Il faut attendre", temps_attente, "seconde.")


identifiants = lire_identifiants()
comptes = obtenir_comptes(identifiants)

processus_légers = []
for compte in comptes:
    processus = threading.Thread(target=lancer_robot, args=(compte, 0))
    processus.start()
    processus_légers.append(processus)

for processus in processus_légers:
    processus.join()
