"""
Auteur : Corentin Bocquillon <corentin@nybble.fr>

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ce fichier permet de faire le calcul des ressources nécessaires.
"""


def obtenir_coût_bâtiment(nom_bâtiment, niveau):
    coût = []

    if (nom_bâtiment == "forêt"):
        coût.append(int(60 * 1.5 ** (niveau - 1)))
        coût.append(int(15 * 1.5 ** (niveau - 1)))
        coût.append(0)
        coût.append(int(10 * niveau * 1.1 ** niveau))

    elif (nom_bâtiment == "mine de métal"):
        coût.append(int(48 * 1.6 ** (niveau - 1)))
        coût.append(int(24 * 1.6 ** (niveau - 1)))
        coût.append(0)
        coût.append(int(10 * niveau * 1.1 ** niveau))

    elif (nom_bâtiment == "fabrique de poudre à canon"):
        coût.append(int(225 * 1.5 ** (niveau - 1)))
        coût.append(int(75 * 1.5 ** (niveau - 1)))
        coût.append(0)
        coût.append(int(20 * niveau * 1.1 ** niveau))

    elif (nom_bâtiment == "quartier de travailleurs"):
        coût.append(int(75 * 1.5 ** (niveau - 1)))
        coût.append(int(30 * 1.5 ** (niveau - 1)))
        coût.append(0)
        coût.append(0)

    elif (nom_bâtiment == "chantier naval"):
        coût.append(int(400 * 2 ** (niveau - 1)))
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(int(100 * 2 ** (niveau - 1)))
        coût.append(0)

    elif (nom_bâtiment == "camp de formation de travailleurs"):
        coût.append(int(400 * 2 ** (niveau - 1)))
        coût.append(int(120 * 2 ** (niveau - 1)))
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(0)

    elif (nom_bâtiment == "laboratoire de recherche"):
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(int(400 * 2 ** (niveau - 1)))
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(0)

    elif (nom_bâtiment == "taverne"):
        coût.append(int(2000000 * 2 ** (niveau - 1)))
        coût.append(int(1000000 * 2 ** (niveau - 1)))
        coût.append(int(500000 * 2 ** (niveau - 1)))
        coût.append(0)

    return coût


def obtenir_coût_recherche(nom, niveau):
    coût = []
    if nom == "espionnage":
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(int(1000 * 2 ** (niveau - 1)))
        coût.append(int(200 * 2 ** (niveau - 1)))

    elif nom == "canons":
        coût.append(int(800 * 2 ** (niveau - 1)))
        coût.append(int(200 * 2 ** (niveau - 1)))
        coût.append(0)

    elif nom == "voilure":
        coût.append(int(2000 * 2 ** (niveau - 1)))
        coût.append(int(4000 * 2 ** (niveau - 1)))
        coût.append(int(600 * 2 ** (niveau - 1)))

    elif nom == "coque":
        coût.append(int(1000 * 2 ** (niveau - 1)))
        coût.append(int(100 * 2 ** (niveau - 1)))
        coût.append(0)

    elif nom == "colonisation":
        coût.append(int(4000 * 2 ** (niveau - 1)))
        coût.append(int(8000 * 2 ** (niveau - 1)))
        coût.append(int(4000 * 2 ** (niveau - 1)))

    elif nom == "formation_officiers":
        coût.append(int(0 * 2 ** (niveau - 1)))
        coût.append(int(400 * 2 ** (niveau - 1)))
        coût.append(int(600 * 2 ** (niveau - 1)))

    coût.append(0)
    return coût


def obtenir_temps_recherche(coût, niveau_laboratoire):
    temps = coût[0] + coût[1] + coût[2]
    temps /= (1000 * (1 + niveau_laboratoire))
    temps *= 3600
    temps = int(temps)
    return temps


def obtenir_temps_construction(coût, niveau_camp_formation_travailleurs,
                               niveau_taverne):
    temps = coût[0] + coût[1] + coût[2]
    temps /= (2500 * (1 + niveau_camp_formation_travailleurs))
    temps *= 3600
    temps /= (2 ** niveau_taverne)
    temps = int(temps)

    return temps


def fabriquer_chaîne_ressources(coût, temps):
    chaîne = "Bois : " + str(coût[0])
    chaîne += ", Métal : " + str(coût[1])
    chaîne += ", Poudre à canon : " + str(coût[2])
    chaîne += ", Travailleurs : " + str(coût[3])
    chaîne += ", Temps : " + str(temps) + "s"

    return chaîne


def obtenir_production_bois(niveau):
    return int(60.0 * niveau * 1.1**niveau)


def obtenir_production_métal(niveau):
    return int(40.0 * niveau * 1.1**niveau)


def obtenir_production_pàc(niveau):
    return int(20.0 * niveau * 1.1**niveau)


def obtenir_production_travailleurs(niveau):
    return int(20.0 * niveau * 1.1**niveau)


def obtenir_production_totale(bâtiments):
    production_totale = []
    production_bois = obtenir_production_bois(bâtiments[0])
    production_métal = obtenir_production_bois(bâtiments[1])
    production_pàc = obtenir_production_bois(bâtiments[2])
    production_totale.append(production_bois)
    production_totale.append(production_métal)
    production_totale.append(production_pàc)
    return production_totale


def obtenir_temps_production(ressources_manquantes, bâtiments):
    production = obtenir_production_totale(bâtiments)
    temps_le_plus_long = 0

    for i in range(len(production)):
        if ressources_manquantes[i] <= 0:
            continue
        temps = ressources_manquantes[i] / production[i]
        temps *= 3600
        if temps_le_plus_long < temps:
            temps_le_plus_long = temps

    return temps_le_plus_long
