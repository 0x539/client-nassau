"""
Auteur : Baptiste Bocquillon <pyth0n11@nybble.fr> 2016 à 2017
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016 à 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Permet d’obtenir les identifiants du joueur.
"""

import réseau

import sys
import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GObject


def lire_identifiants():
    try:
        fichier = open("compte", "r")
        identifiants = fichier.readline().rstrip().split()
        if identifiants[0] != "" and identifiants[1] != "":
            return identifiants
        else:
            raise "Exception !"
    except:
        return False


def obtenir_identifiants(adresse_serveur):
    identifiants = lire_identifiants()

    if identifiants is False:
        try:
            fen = Connexion(adresse_serveur)
            identifiants = fen.identifiants
            return identifiants
        except:
            sys.exit(0)
    else:
        return identifiants


class Connexion:
    def __init__(self, adresse_serveur):
        self.constructeur = Gtk.Builder()
        self.constructeur.add_from_file('ihm/connexion.glade')

        self.fenêtre = self.constructeur.get_object("fenêtre_connexion")
        self.fenêtre.show_all()

        self.adresse_serveur = adresse_serveur
        self.obtenir_zones_de_saisies()

        self.constructeur.connect_signals(self)

        Gtk.main()

    def obtenir_zones_de_saisies(self):
        self.pseudo = self.constructeur.get_object("pseudo")
        self.mot_de_passe = self.constructeur.get_object("mot_de_passe")

    def afficher_avertissement(self, titre="Erreur.", texte_bouton="Réessayer",
                               message=""):
            fenêtre_avertissement = Gtk.Dialog(titre, self.fenêtre)
            fenêtre_avertissement.add_button(texte_bouton, 1)

            conteneur = fenêtre_avertissement.get_content_area()
            conteneur.add(Gtk.Label(message))

            fenêtre_avertissement.show_all()
            fenêtre_avertissement.run()
            fenêtre_avertissement.destroy()

    def remplir_identifiants(self):
        self.identifiants = []
        self.identifiants.append(self.pseudo.get_text())
        self.identifiants.append(self.mot_de_passe.get_text())

    def se_connecter(self, bouton=None):
        self.remplir_identifiants()

        if réseau.vérifier_identifiants(self.adresse_serveur,
                                        self.identifiants) is not False:
            self.sauvegarder_identifiants()
            self.fenêtre.destroy()
            self.fermeture_fenêtre()
        else:
            del self.identifiants
            titre = "Une erreur est survenue lors de la connexion."
            message = "Identifiants incorrects !"
            self.afficher_avertissement(titre, "Réessayer", message)

    def créer_un_compte(self, bouton):
        self.remplir_identifiants()
        retour = réseau.créer_compte(self.identifiants,
                                     self.adresse_serveur[0], 4285)
        if retour is False:
            del self.identifiants
            self.afficher_avertissement("Erreur.", "Réessayer",
                                        "Erreur inconnu.")
            return
        elif not isinstance(retour, list):
            del self.identifiants
            self.afficher_avertissement("Erreur.", "Réessayer",
                                        retour)
            return

        self.interface_création_compte = retour[0]

        self.fenêtre_code = self.constructeur.get_object("fenêtre_code")
        image = self.constructeur.get_object("image")
        image.set_from_file(retour[1])
        self.fenêtre_code.show_all()

    def vérifier_code(self, bouton):
        champ = self.constructeur.get_object("code")
        try:
            code = champ.get_text()
            réseau.confirmer_inscription(self.interface_création_compte, code)
            self.fenêtre_code.destroy()
            self.afficher_avertissement("Information", "D’accord",
                                        "Création de compte réussi.")
            self.se_connecter()
        except:
            del self.identifiants
            self.fenêtre_code.destroy()
            self.afficher_avertissement("Échec.", "Réessayer",
                                        "Échec de la création du compte.")

    def fermer_fenêtre_connexion(self, *args):
        del self.identifiants

    def sauvegarder_identifiants(self):
        fichier = open("compte", "w")
        ligne = self.identifiants[0] + " " + self.identifiants[1] + "\n"
        fichier.write(ligne)

    def fermeture_fenêtre(self, *args):
        Gtk.main_quit(*args)
