"""
Auteur : Corentin Bocquillon <corentin@nybble.fr> 2016 à 2017

Ce logiciel est régi par la licence CeCILL soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA
sur le site « http://www.cecill.info ».

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme,  le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques
associés au chargement,  à l'utilisation,  à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant
donné sa spécificité de logiciel libre, qui peut le rendre complexe à
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant  des  connaissances  informatiques approfondies.  Les
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement,
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez
pris connaissance de la licence CeCILL, et que vous en avez accepté les
termes.

Ceci est le fichier contenant les fonctions permettant de dialoguer en réseau.
"""

import locale
import socket
import time
import datetime
import json
import threading
import os


class Serveur ():
    def __init__(self, adresse, pseudo, mot_de_passe):
        locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

        self.adresse = adresse
        self.pseudo = pseudo
        self.mot_de_passe = mot_de_passe

        self.mes_îles = self.obtenir_informations_sur_mes_îles()
        if self.mes_îles is not None:
            self.mes_îles = json.loads(self.mes_îles)
            self.nombre_îles = self.mes_îles["nombre d’îles"]

        self.demander_niveau_bâtiments()
        self.ressources = self.__calculer_ressources__(True)
        self.date_dernière_màj = time.time()

    def créer_interface_connexion(self):
        return socket.socket(socket.AF_INET,
                             socket.SOCK_DGRAM)

    def récupérer_ressources(self, île=1, mettre_à_jour=False):
        ressources = self.__calculer_ressources__(mettre_à_jour)
        return ressources[île - 1]

    def __calculer_ressources__(self, mettre_à_jour=False):
        if mettre_à_jour:
            self.__demander_ressources__()
            return self.ressources

        ressources = []
        for i in range(self.nombre_îles):
            maintenant = time.time()
            temps_écoulé = maintenant - self.date_dernière_màj

            gain_bois = 60 * int(self.bâtiments[i][0]) * 1.1\
                ** int(self.bâtiments[i][0]) / 3600.0
            gain_métal = 40 * int(self.bâtiments[i][1]) * 1.1\
                ** int(self.bâtiments[i][1]) / 3600.0
            gain_poudre_à_canon = 20 * int(self.bâtiments[i][2]) * 1.1\
                ** int(self.bâtiments[i][2]) / 3600.0

            ressources_de_l_île = []

            ressources_de_l_île.append(
                str(int(self.ressources[i][0]) + int(gain_bois * temps_écoulé)))
            ressources_de_l_île.append(
                str(int(self.ressources[i][1]) + int(gain_métal * temps_écoulé)))
            ressources_de_l_île.append(
                str(int(self.ressources[i][2]) + int(gain_poudre_à_canon * temps_écoulé)))
            ressources_de_l_île.append(self.ressources[i][3])
            ressources.append(ressources_de_l_île)

        return ressources

    def __demander_ressources__(self):
        entête = self.__obtenir_entête__()
        self.ressources = []
        processus_légers = []
        verrou = threading.Lock()

        # On demande les ressources
        for i in range(1, self.nombre_îles + 1):
            processus = threading.Thread(target=self.__demander_ressources_sur_île__,
                                         args=(i, verrou, self.ressources))
            processus.start()
            processus_légers.append(processus)

        # On attend de toutes les obtenir.
        for processus in processus_légers:
            processus.join()

        # On les trient par île.
        self.ressources.sort(key=lambda liste: liste[len(liste) - 1])

        # On supprime le numéro des îles dans la liste.
        for i in range(len(self.ressources)):
            j = self.ressources[i]
            j = j[:len(j) - 1]

        self.date_dernière_màj = time.time()

    def __demander_ressources_sur_île__(self, île, verrou, liste):
        message = self.__obtenir_entête__()
        message += " ressources " + str(île)

        réponse = self.envoyer_et_recevoir_message(message)
        if réponse is not False:
            ressources = réponse.split(" ")
            ressources.append(île)

            verrou.acquire()
            liste.append(ressources)
            verrou.release()

    def __obtenir_entête__(self):
        return self.pseudo + " " + self.mot_de_passe

    def demander_niveau_bâtiments(self):
        entête = self.__obtenir_entête__()
        self.bâtiments = []

        processus_légers = []
        verrou = threading.Lock()
        for i in range(1, self.nombre_îles + 1):
            processus_léger = threading.Thread(target=self.demander_niveau_bâtiments_île,
                                               args=(i, verrou, self.bâtiments))
            processus_léger.start()
            processus_légers.append(processus_léger)

        for processus in processus_légers:
            processus.join()

        # On tri la liste pour avoir les îles dans l’ordre.
        self.bâtiments.sort(key=lambda liste: liste[11])

        # On supprime le numéro des îles dans la liste.
        for i in range(len(self.bâtiments)):
            self.bâtiments[i] = self.bâtiments[i][:len(self.bâtiments[i]) - 1]

        return self.bâtiments

    def demander_niveau_bâtiments_île(self, île, verrou, liste):
        entête = self.__obtenir_entête__()
        message = entête
        message += " bâtiments " + str(île)

        bâtiments = self.envoyer_et_recevoir_message(message).split(" ")
        bâtiments.append(île)

        verrou.acquire()
        liste.append(bâtiments)
        verrou.release()

    def demander_nombre_navires(self, numero_ile=1):
        message = self.__obtenir_entête__()
        message += " navires " + str(numero_ile)
        self.navires = self.envoyer_et_recevoir_message(message).split(" ")
        return self.navires

    def demander_nombre_défenses(self, numero_ile=1):
        message = self.__obtenir_entête__()
        message += " défenses " + str(numero_ile)
        self.défenses = self.envoyer_et_recevoir_message(message).split(" ")
        return self.défenses

    def construire_bâtiment(self, numero_ile, nom):
        self.y_a_t_il_une_construction_en_cours(numero_ile)
        if self.construction_en_cours:
            return

        message = self.__obtenir_entête__()
        message += " construire_bâtiment " + str(numero_ile)
        message += " " + nom
        résultat = self.envoyer_et_recevoir_message(message).split(" ")
        self.__demander_ressources__()

        return résultat

    def construire_navires(self, numero_ile, nom, nombre):
        # À faire, vérifier qu’il n’y a pas de navires en construction
        message = self.__obtenir_entête__()
        message += " construire_navires " + str(numero_ile)
        message += " " + nom + " " + str(nombre)

        résultat = self.envoyer_et_recevoir_message(message).split(" ")
        self.__demander_ressources__()

        return résultat

    def construire_défenses(self, numero_ile, nom, nombre):
        # À faire, vérifier qu’il n’y a pas de défenses en construction
        message = self.__obtenir_entête__()
        message += " construire_défenses " + str(numero_ile)
        message += " " + nom + " " + str(nombre)

        résultat = self.envoyer_et_recevoir_message(message).split(" ")

        if résultat == "incorrecte":
            print("Erreur lors de la construction de ", nom, ".")
        self.__demander_ressources__()

        return résultat

    def rechercher(self, numéro_île, nom):
        message = self.__obtenir_entête__()
        message += " lancer_recherche " + nom
        message += " " + str (numéro_île)

        résultat = self.envoyer_et_recevoir_message(message)

        if résultat == "incorrecte":
            return False
        self.__demander_ressources__()

        return résultat

    def y_a_t_il_une_construction_en_cours(self, numero_ile, lisible=True):
        message = self.__obtenir_entête__()
        message += " bâtiment_en_cours_de_construction " + str(numero_ile)
        résultat = self.envoyer_et_recevoir_message(message).split(" ")

        if résultat[0] == "incorrecte":
            self.construction_en_cours = False
            return False

        elif résultat[0] == "oui":
            self.construction_en_cours = True
            résultat_retourné = []
            self.bâtiment_en_cours_de_construction = convertir_code_réseau_vers_nom_bâtiment(
                résultat[1])
            résultat_retourné.append(self.bâtiment_en_cours_de_construction)

            if lisible:
                self.heure_fin_construction = time.strftime(
                    "%A %d %B %Y à %X", time.localtime(int(résultat[2])))
                résultat_retourné.append(self.heure_fin_construction)
            else:
                résultat_retourné.append(int(résultat[2]))

            return résultat_retourné

        elif résultat[0] == "non":
            self.construction_en_cours = False
            return False

    def y_a_t_il_une_construction_de_navires_en_cours(self, numero_ile,
                                                      lisible=True):
        message = self.__obtenir_entête__()
        message += " navires_en_cours_de_construction " + str(numero_ile)
        résultat = self.envoyer_et_recevoir_message(message).split(" ")

        if résultat[0] == "incorrecte":
            return False

        elif résultat[0] == "oui":
            résultat_retourné = []
            résultat_retourné.append(convertir_code_réseau_vers_nom_navire
                                     (résultat[1]))
            résultat_retourné.append(résultat[2])

            if lisible:
                résultat_retourné.append(time.strftime("%A %d %B %Y à %X",
                                                       time.localtime
                                                       (int(résultat[3]))))
            else:
                résultat_retourné.append(int(résultat[3]))

            return résultat_retourné

        elif résultat[0] == "non":
            return False

    def y_a_t_il_une_construction_de_défenses_en_cours(self, numero_ile,
                                                       lisible=True):
        message = self.__obtenir_entête__()
        message += " défenses_en_cours_de_construction " + str(numero_ile)
        résultat = self.envoyer_et_recevoir_message(message).split(" ")

        if résultat[0] == "incorrecte":
            return False

        elif résultat[0] == "oui":
            résultat_retourné = []
            résultat_retourné.append(résultat[1])
            résultat_retourné.append(résultat[2])

            if lisible:
                résultat_retourné.append(time.strftime("%A %d %B %Y à %X",
                                                       time.localtime
                                                       (int(résultat[3]))))
            else:
                résultat_retourné.append(int(résultat[3]))

            return résultat_retourné

        elif résultat[0] == "non":
            return False

    def y_a_t_il_une_recherche_en_cours(self, lisible=True):
        message = self.__obtenir_entête__()
        message += " recherches_en_cours"
        résultat = self.envoyer_et_recevoir_message(message).split(" ")

        if résultat[0] == "incorrecte":
            return False

        elif résultat[0] == "oui":
            résultat_retourné = []
            résultat_retourné.append(résultat[1])

            if lisible:
                résultat_retourné.append(time.strftime("%A %d %B %Y à %X",
                                                       time.localtime
                                                       (int(résultat[2]))))
            else:
                résultat_retourné.append(int(résultat[2]))

            return résultat_retourné

        elif résultat[0] == "non":
            return False

    def obtenir_constructions_en_cours(self):
        constructions = []
        verrou_constructions = threading.Lock()
        liste_processus_légers = []

        for i in range(1, self.nombre_îles + 1):
            processus_léger = threading.Thread(target=self.obtenir_constructions_en_cours_île,
                                               args=(i, verrou_constructions, constructions))
            liste_processus_légers.append(processus_léger)
            processus_léger.start()

        for processus in liste_processus_légers:
            processus.join()

        # On tri la liste par île.
        constructions.sort(key=lambda liste: liste[3])

        # On supprime les numéros d’île.
        for i in range(len(constructions)):
            constructions[i] = constructions[i][:3]

        return constructions

    def obtenir_constructions_en_cours_île(self, numéro_île, verrou, liste):
        construction = []
        construction.append(
            self.y_a_t_il_une_construction_en_cours(numéro_île, False))
        construction.append(self.y_a_t_il_une_construction_de_navires_en_cours(numéro_île,
                                                                               False))
        construction.append(self.y_a_t_il_une_construction_de_défenses_en_cours(numéro_île,
                                                                                False))
        construction.append(numéro_île)

        verrou.acquire()
        liste.append(construction)
        verrou.release()

    def demander_informations_secteur(self, ocean, secteur):
        message = self.__obtenir_entête__() + " informations_îles "
        message += str(ocean) + " " + str(secteur)

        résultat = self.envoyer_et_recevoir_message(message).split(";")
        if résultat[0] == "incorrecte":
            return []

        îles = []
        for i in range(0, 30, 2):
            îles.append([résultat[i], résultat[i + 1]])

        return îles

    def envoyer_et_recevoir_message(self, message):
        # Réception
        essai = 0
        succès = False

        while essai != 10 or not succès:
            try:
                interface = self.créer_interface_connexion()
                interface.settimeout(2)
                essai += 1

                # Envoi.
                interface.sendto(bytes(message, "utf-8"),
                                 self.adresse)

                # Réception.
                données = interface.recv(8192).decode("utf-8")
                succès = True
                interface.close()
                return données
            except:
                print("Le serveur ne répond pas.")

        return False

    def obtenir_nombre_messages(self):
        message = self.__obtenir_entête__()
        message += " nombre_messages"
        réponse = self.envoyer_et_recevoir_message(message)
        if réponse == "incorrecte":
            return None
        else:
            return int(réponse)

    def obtenir_contenu_message(self, numéro):
        message = self.__obtenir_entête__()
        message += " consulter_message " + str(numéro)
        réponse = self.envoyer_et_recevoir_message(message)
        if réponse == "incorrecte":
            return None
        else:
            return réponse

    def lancer_expédition(self, flotte, numéro_île_départ, type_expédition,
                          numéro_île_arrivée=None, océan=None, secteur=None,
                          emplacement=None, ressources=None):
        # S’il n’y a aucun navires, on ne fait rien.
        aucun_navire = True
        for i in flotte:
            if i > 0:
                aucun_navire = False
                break
        if aucun_navire:
            return False

        types = {"Attaquer": "attaque", "Transporter": "transporter"}
        types["Stationner"] = "stationnement"
        types["Coloniser"] = "colonisation"
        types["Espionner"] = "espionnage"
        if type_expédition not in types:
            return False
        if type_expédition == "Transporter" and ressources is None:
            return False
        elif type_expédition == "Coloniser" and ressources is None:
            return False
        elif type_expédition == "Stationner" and\
             (numéro_île_arrivée is None or numéro_île_arrivée == 0):
            return "Le numéro de l’île d’arrivée est incorrecte."

        message = self.__obtenir_entête__()
        message += " " + types[type_expédition] + " "
        message += str(numéro_île_départ) + " "

        if type_expédition == "Stationner":
            message += str(numéro_île_arrivée)
        else:
            message += str(océan) + " " + str(secteur) + " "
            message += str(emplacement)

        if type_expédition == "Espionner":
            if flotte[0] == 0:
                return False
            message += " " + str(flotte[0])

        elif type_expédition == "Coloniser":
            if flotte[2] <= 0:
                return False
            message += " " + str(flotte[2]) + " " + str(flotte[1])
            if ressources is None:
                message += " 0 0 0"
        else:
            message += " " + str(flotte[0]) + " " + str(flotte[1]) + " " + str(flotte[2]) + " "\
                       + str(flotte[3]) + " " + str(flotte[4]) + " " + str(flotte[5]) + " "\
                       + str(flotte[6]) + " "\
                       + str(flotte[7]) + " " + str(flotte[8])

        if (type_expédition == "Transporter" or type_expédition == "Coloniser"\
           or type_expédition == "Stationner") and ressources is not None:
            message += " " + str(ressources["bois"]) + " " + str(ressources["métal"])\
                       + " " + str(ressources["pàc"])

        réponse = self.envoyer_et_recevoir_message(message)
        if réponse != "fait":
            print("Erreur lors du lancement de l’expédition.")
            return False
        else:
            return True

    def obtenir_informations_sur_mes_îles(self):
        message = self.__obtenir_entête__()
        message += " mes_îles"
        réponse = self.envoyer_et_recevoir_message(message)
        return réponse

    def coloniser_île(self, île_départ, océan, secteur, emplacement, nb_btlo, nb_btlé):
        message = self.__obtenir_entête__()
        message += " colonisation " + île_départ + " " + océan + " " + secteur
        message += " " + emplacement + " " + nb_btlo + " " + nb_btlé
        réponse = self.envoyer_et_recevoir_message(message)
        if réponse == "incorrecte":
            return false
        elif réponse == "fait":
            return true

    def obtenir_expéditions(self):
        message = self.__obtenir_entête__()
        message += " expéditions"

        réponse = self.envoyer_et_recevoir_message(message)
        if réponse is False or réponse == "aucune":
            return False
        réponse = réponse.split("\n")

        documents = []
        for document in réponse:
            documents.append(json.loads(document))

        return documents

    def obtenir_niveau_recherches(self):
        message = self.__obtenir_entête__()
        message += " consulter_recherches"

        réponse = self.envoyer_et_recevoir_message(message)
        if réponse is False or réponse == "incorrecte":
            return False
        réponse = réponse.split()
        return réponse


def convertir_code_réseau_vers_nom_bâtiment(code_réseau):
    nom_bâtiment = ""

    if code_réseau == "forêt":
        nom_bâtiment = "forêt"
    elif code_réseau == "mine_de_métal":
        nom_bâtiment = "mine de métal"
    elif code_réseau == "fabrique_poudre_canon":
        nom_bâtiment = "fabrique de poudre à canon"
    elif code_réseau == "quartier_travailleurs":
        nom_bâtiment = "quartier de travailleurs"
    elif code_réseau == "chantier_naval":
        nom_bâtiment = "chantier naval"
    elif code_réseau == "camp_formation_travailleurs":
        nom_bâtiment = "camp de formation de travailleurs"
    elif code_réseau == "laboratoire_recherche":
        nom_bâtiment = "laboratoire de recherche"
    elif code_réseau == "taverne":
        nom_bâtiment = "taverne"

    return nom_bâtiment


def convertir_code_réseau_vers_nom_navire(code_réseau):
    nom_navire = ""

    if code_réseau == "bateau_espion":
        nom_navire = "bateau espion"
    elif code_réseau == "bateau_transport_léger":
        nom_navire = "bateau de transport léger"
    elif code_réseau == "bateau_transport_lourd":
        nom_navire = "bateau de transport lourd"
    elif code_réseau == "canonnière":
        nom_navire = "canonnière"
    elif code_réseau == "corvette":
        nom_navire = "corvette"
    elif code_réseau == "frégate":
        nom_navire = "frégate"
    elif code_réseau == "brick":
        nom_navire = "brick"
    elif code_réseau == "galion":
        nom_navire = "galion"
    elif code_réseau == "navire_de_ligne":
        nom_navire = "navire de ligne"

    return nom_navire


def vérifier_identifiants(adresse, identifiants):
    message = identifiants[0] + " "
    message += identifiants[1] + " ressources 1"

    succès = False
    essai = 0
    while essai != 10 or not succès:
        try:
            interface = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            interface.settimeout(2)
            essai += 1

            interface.sendto(bytes(message, "utf-8"), adresse)

            données = interface.recv(8192).decode("utf-8")
            if données != "incorrecte":
                succès = True
            interface.close()
            return succès
        except:
            pass

        return False


def créer_compte(identifiants, adresse, port):
    # Formation du message.
    message = "créer_compte "
    message += identifiants[0] + " " + identifiants[1]

    # Ouverture d’une connexion.
    interface = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    interface.connect ((adresse, port))

    # Envoi du message.
    retour = interface.sendall (bytearray (message, "utf-8"))
    if retour is not None:
        return False

    réponse = interface.recv(7).decode("utf-8").rstrip("\0")
    try:
        taille_fichier = int (réponse)
    except:
        return réponse + interface.recv(128).decode("utf-8")

    # Récupération du jeu d’imitation.
    chemin_code = "/tmp/nassau-jeu-imitation-" + str(os.getpid()) + ".png"
    fichier = open (chemin_code, "wb")
    total_reçu = 0
    image = bytes()
    while total_reçu < taille_fichier:
        reçu = interface.recv(taille_fichier)
        image += reçu
        total_reçu += len (reçu)

    fichier.write (image)
    fichier.close ()

    # On retourne l’interface pour pouvoir confirmer l’inscription plus tard.
    return [interface, chemin_code]

def confirmer_inscription(interface, code):
    # Envoi du message.
    retour = interface.sendall (bytearray (code, "utf-8"))
    if retour is not None:
        return False

    # Récupération de la réponse.
    réponse = interface.recv(128).decode("utf-8")
    return réponse
